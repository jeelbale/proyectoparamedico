import React, { Component } from 'react';

import styled from "styled-components"

import {SHADOWS2} from "../Constantes"



import { bindActionCreators } from "redux";
import { connect } from "react-redux";




const Form = styled.div`
padding:2rem;
  position: absolute;
  top: 3rem;
  right: -3rem;
  width: 60%;
  text-align: center;
  border-radius: 2px;
  margin: 0 auto;
  background: white;
  z-index: 9999; 
  ${SHADOWS2}
`;


class App extends Component {

constructor(props){
  super(props)
  this.state = {
    text: "",
    user:0,
    error: ""
  }
}


submit(){


  if(this.state.text){
  //this.props.push({task:this.state.text, user: this.state.user})
  
  this.setState({"text":"", error:""})
  }
  else
    this.setState({"text":"", error:"Error: No escribiste la tare..."})
}



  render() {
 const {persons} = this.props
    return (
      <Form>
        <h3>Ingresa la tarea</h3>
        
        <input 
          placeholder="Tarea"
          value={this.state.text}
          onChange={(e)=>{this.setState({text:e.target.value})}}
         />
         <br/>

         <select 
          onChange={(ref)=>{this.setState({user:ref.target.value})}}>
          {persons.map((person, i)=>
            <option value={i}>{person.name}</option>)
          }
        </select>
        <br/><br/>

        <button
          onClick={this.submit.bind(this)}
        >Add</button>



           <a href={`/app`}>Atras</a>



        <br/>
        {this.state.error && <label style={{color:"red", 
        "font-size":"12px"}}>{this.state.error}</label>}
         </Form>
    );
  }
}


function mapStateToProps(store) {
  return {

    persons: store.todo.persons.data

  };
}




export default connect(mapStateToProps)(App);


    