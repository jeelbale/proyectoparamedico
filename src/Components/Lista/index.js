import React, { Component } from 'react';
import Li from "./Item" 
class App extends Component {
  constructor(props){
  super(props)
}

  render() {
    return (
      <ul>
       {this.props.items.map((item, i)=>(
        <Li 
          key={i} 
          id={i} 
          update={this.props.update} 
          persona={this.props.persona[item.user]} 
          item={item}/>

        ))}
      </ul>
    );
  }
}

export default App;
