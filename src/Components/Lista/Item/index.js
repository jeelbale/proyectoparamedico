import React, { Component } from 'react';

import styled from 'styled-components';

import * as actionsTodo from "../../../redux/Actions/todo";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";



const H4 = styled.label`
  display: none;
`
const Text = styled.label`
&:hover{

  h4{
     display: none;
  }
  ${H4}{
    display: block;
  }
}



`





class App extends Component {
  constructor(props){
  super(props)
  this.state = {
   edit: false,
   text: this.props.item.task,
  }
}

  update(){
    this.props.update(this.props.id, {task:this.state.text, user:this.props.item.user})
    this.setState({ edit:false})
  }

  render() {
    const {text,edit} = this.state


    //console.log(this.props)
    return (
         <li
         onClick={()=>{
          this.setState({edit: true})}}>
    
      {!edit?
       
<Text>
        <h4>{text}</h4> 
        
        <H4 >{this.props.persona.name}</H4>

        </Text>
        :
        <form 
          onSubmit={this.update.bind(this)}>
         <input
          value={text}
          onChange={(e)=>{this.setState({text:e.target.value})}}
          />
        </form>}
      </li>)

  }
}




function mapStateToProps(store) {
  return {
    taskList: store.todo.tareas
  };
}



function mapDispatchToProps(dispatch) {
  return {

      actionsForTodo: bindActionCreators(actionsTodo, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
