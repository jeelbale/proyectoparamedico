import React, { Component } from 'react';
import styled from "styled-components"

import axios from 'axios';

import Formulario from "./Components/Formulario"
import Formulario2 from "./Components/Formulario2"

import Lista from "./Components/Lista"

import {SHADOWS1, SHADOWS2} from "./Constantes"


import * as actionsTodoServer from "./redux/Actions/todoFromServer"

import { bindActionCreators } from "redux";
import { connect } from "react-redux";


import { Route, Link } from "react-router-dom";


const Card = styled.div`
  position: relative;

  top: 3rem;
  position: relative
  width: 40%;
  border-radius: 2px;
  margin: 0 auto;
  background: white;
  height: 600px;
  ${SHADOWS2}
`;


const Header = styled.div`
  padding:2rem;
  position: relative;
  padding-bottom: 0;
`;

const Button = styled(Link)`
 ${SHADOWS1}
  position: absolute;
  background:#ff5055
  width: 60px;
  height: 60px;
  border-radius: 100%;
  border: 0;
  right: 1rem;
  bottom:-56px;
  z-index: 9999;
  color: white;
  font-size: 30px;
`;
const Title = styled.label`
font-size: 33px;
color: #7100e5;
`;
const Subtitle = styled.label`
font-size: 28px;
color: #7100e5;
opacity: 0.5;
`;

const Text = styled.label`
font-size: 16px;
color: #7100e5;
opacity: 0.3;

${props=>props.tarea &&

`
float: right;


` 

}
`;



const Divider = styled.div`
  position: relative;
  display:block;
  margin: 2rem 0;
  height: 5px;
  border-bottom: 0.5px solid rgba(0,0,0,0.2);
`;

 class App extends Component {
  constructor(props){
  super(props)
  this.state = {
    tareas: [],
    persons:[],
    loading: true,
    showForm: false,
  }


  this.props.actions.fetchPosts()
}


update(id, obj){
  const tmp = this.state.tareas
  tmp[id] = obj
  
  this.setState({tareas: tmp})
}
  
  push(item){
    let tmp = this.state.tareas
    tmp.push(item);
    this.setState({tareas: tmp}) 
  }


  render() {


    if(this.props.loading)
      return <h1>loading...</h1>

    return (
      <Card>
      <Header>
        <Title>Monday,</Title><Subtitle>16th</Subtitle> <Text tarea>{this.state.tareas.length} Task</Text>
        <br/>
        <br/>
        <Text>April</Text>

        <Button to={`${this.props.match.url}/formulario2`}>+</Button>
      </Header>
      <Divider/>


       <Route 
       path={`${this.props.match.url}/formulario`}
       render={(props)=>
        <Formulario 
          {...props}
          ejemplo={{color:"red",size:2}}
          close={()=>{this.setState({showForm:false})}}
          push={this.push.bind(this)}
          persona={this.props.persons}
        />}

      />


       <Route 
       path={`${this.props.match.url}/formulario2`}
       
       component={Formulario2}
      />




        <Lista
          update={this.update.bind(this)}
          items={this.state.tareas}
          persona={this.props.persons}
        />


    
     
      </Card>

    );
  }
}

//export default App;


function mapStateToProps(store) {
  return {
    loading: store.todo.persons.loading,
    persons: store.todo.persons.data
  };
}



function mapDispatchToProps(dispatch) {
  return {

      actions: bindActionCreators(actionsTodoServer, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(App);

