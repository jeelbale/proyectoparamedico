import axios from "axios"

function requestPosts() {
  return {
    type: "REQUEST_PERSONS",
  }
}


function receivePosts(data) {
  return {
    type: "RECEIVE_PERSONS",
    payload: data
  }
}


export function fetchPosts() {

  return function (dispatch) {

    dispatch(requestPosts())



    return axios.get(`http://localhost:8080/api/users`)
      .then(
        response => response.data,
        error => {
          alert("Error feching data, No hay conexcion o algo por esl estilo");
          return []
        }
      ).then(data => dispatch(receivePosts(data)))
  }
}