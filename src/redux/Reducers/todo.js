export default function reducer(
  state = {
  tareas: [],
  

  persons:{
    data: [],
    loading: false,

  }
 
  }, action) {

    switch (action.type) {
      
      case "AGREGAR_TAREAS": {
          return {...state, 
            tareas:action.payload}
      }

      case "REQUEST_PERSONS":{
    
        const persons = {...state.persons,  loading:true}
        return {...state, 
            persons:persons
          }
      }


       case "RECEIVE_PERSONS":{
        const persons = {...state.persons,  loading:false, data:action.payload}
        return {...state, 
            persons:persons

          }
      }


      
    }

  return state
}

 