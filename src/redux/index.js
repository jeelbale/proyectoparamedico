import { createStore, applyMiddleware } from "redux";

import thunk from 'redux-thunk';

import reducer from "./Reducers";

//import {loadState, saveState} from "../tools/db/localStorage"
const Root = () => {
	const initialState = undefined; //loadState()


	const store = createStore(reducer, applyMiddleware(thunk));

	return store;
};

export default Root;
